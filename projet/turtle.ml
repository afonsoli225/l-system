open Stack

type command =
| Line of int
| Move of int
| Turn of int
| Store
| Restore

type position = {
  x: float;      (** position x *)
  y: float;      (** position y *)
  a: int;        (** angle of the direction *)
}


(** Put here any type and function implementations concerning turtle *)

let pi = 2.0 *. asin 1.0;;
let radian a= float (a) *. pi /. 180.;;

(**turn angle: ajouter un certain degré angle à l'angle de la position c1 *) 
let turn c1 angle = 
 	{x=c1.x;y=c1.y;a=(c1.a+angle) mod 360};;

(**move length : avancer de length unités et renvoyer la position modifiée *) 
let move c1 length =  
	let p={x=c1.x+. cos(radian(c1.a))*. float(length);y=c1.y+. sin(radian(c1.a))*. float(length);a=c1.a} in 
  	Graphics.moveto (Float.to_int p.x) (Float.to_int p.y);
  	p;;

(**line length : avancer de length unités, en traçant un segment et renvoyer la position modifiée *) 
let line c1 length =
	let p={x=c1.x+. cos(radian(c1.a))*. float(length);y=c1.y+. sin(radian(c1.a))*. float(length);a=c1.a} in
  	Graphics.lineto (Float.to_int p.x) (Float.to_int p.y);
  	p;;

(** appelle la fonction Stack.push pour ajouter la position pos à pile puis renvoyer la pile modifiée*)
let mypush pos pile = 
	push pos pile;
	Stack.copy pile
;;

(** appelle la fonction Stack.pop sur pile, se déplacer, renvoyer la pile et la position modifiées*)
let mypop (pile,pos) =
	let p = pop pile in 
	Graphics.moveto (Float.to_int p.x) (Float.to_int p.y);
	(pile,p)
;;

(**appel des fonctions adéquates, le graphe diminue en fonction de l'itération, renvoie le double modifié*)
let launch cmd  (pile,pos) iteration  =
	match cmd with
	| Turn r -> (pile,turn pos r)
	| Move r -> if r<15 then 
					(pile,move pos (r/(iteration)+5))
				else
					(pile,move pos (r/(iteration)+2))
    | Line r -> if r<15 then 
					(pile,line pos (r/(iteration)+5))
				else
					(pile,line pos (r/(iteration)+2))
    | Store -> (mypush pos pile,pos)
    | Restore -> (mypop (pile,pos))
;;

let rec parcours_aux l (pile,pos) iteration=
	match l with
	|[] -> (pile,turn pos 0)
	|m::ls -> parcours_aux ls (launch m (pile,pos) iteration) (iteration);
;;

(**prend liste de commandes, crée une pile pour mémoriser, une position courante et boucle*)
let parcours l iteration=
	let pile = Stack.create() in
	let pos = {x = 750. ; y=100. ; a = 90} in
	Graphics.moveto (Float.to_int pos.x) (Float.to_int pos.y);
	parcours_aux l (pile,pos) iteration;
;;
